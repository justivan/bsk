package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testGameShouldReturnAFrame() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(2,7);
		Frame f2 = new Frame(3,6);
		Frame f3 = new Frame(3,5);
		Frame f4 = new Frame(10,0);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(10,0);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(f8, g.getFrameAt(8));
		
	}
	
	@Test
	public void testGameShouldReturnScore() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(2,7);
		Frame f2 = new Frame(3,6);
		Frame f3 = new Frame(3,5);
		Frame f4 = new Frame(6,3);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(5,4);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(84, g.calculateScore());
		
	}
	
	@Test
	public void testEmptyGameShouldReturnScoreZero() throws BowlingException {

		Game g = new Game();
						
		assertEquals(0, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithSpare() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		
		//spare frame
		Frame f1 = new Frame(3,7);
		//should add 3 to the spare result
		Frame f2 = new Frame(3,6);
		
		Frame f3 = new Frame(3,5);
		Frame f4 = new Frame(4,5);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(6,3);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(88, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithStrike() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(2,7);
		Frame f2 = new Frame(3,6);
		Frame f3 = new Frame(3,5);
		
		//Strike frame
		Frame f4 = new Frame(10,0);
		//Strike should add 9 to the strike result
		Frame f5 = new Frame(0,9);
		
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(7,0);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(92, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithStrikeAndSpare() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(2,7);
		Frame f2 = new Frame(3,6);
		Frame f3 = new Frame(3,5);
		
		//Strike frame
		Frame f4 = new Frame(10,0);
		//Should add 10 to the strike result and not 11 (spare result), also 
		//this is the spare Frame
		Frame f5 = new Frame(1,9);
		
		//Spare should add 1 to the spare result: f5 = 10 + 1 = 11 (spare result)
		Frame f6 = new Frame(1,6);
		
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(7,0);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(95, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithStrikeAfterFirstStrike() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(2,7);
		Frame f2 = new Frame(3,6);
		Frame f3 = new Frame(3,5);
		
		//1st Strike frame 
		Frame f4 = new Frame(10,0);
		//2nd Strike frame, should add 10 and 1 (from the subsequent frame) to the 1st strike result
		Frame f5 = new Frame(10,0);
		//Should add 7 to the 2nd strike result
		Frame f6 = new Frame(1,6);
		
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(7,0);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(102, g.calculateScore());
		
	}

	@Test
	public void testGameShouldReturnScoreWithStrikesAfterFirstStrike() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(2,7);
		Frame f2 = new Frame(3,6);
		Frame f3 = new Frame(3,5);
		
		//1st Strike frame 30pts
		Frame f4 = new Frame(10,0);
		//2nd Strike frame, should add 10 and 10 (from the subsequent frame) to the 1st strike result
		//25 pts
		Frame f5 = new Frame(10,0);
		//3rd Strike frame, should add 10 and  (from the subsequent frame) to the 1st strike result
		//18 pts
		Frame f6 = new Frame(10,0);
		//Should add 5 to the 3rd strike result
		Frame f7 = new Frame(5,3);
		
		Frame f8 = new Frame(7,0);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(130, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithSpareAfterFirstSpare() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		
		//1st spare frame
		Frame f1 = new Frame(3,7);
		//2nd spare frame
		Frame f2 = new Frame(4,6);
		
		Frame f3 = new Frame(3,5);
		
		Frame f4 = new Frame(4,5);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(6,3);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(93, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithMultipleSparesAfterFirstSpare() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		
		//1st spare frame
		Frame f1 = new Frame(3,7);
		//2nd spare frame
		Frame f2 = new Frame(4,6);
		//3rd spare frame
		Frame f3 = new Frame(5,5);
		
		Frame f4 = new Frame(4,5);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(6,3);
		Frame f9 = new Frame(0,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		assertEquals(101, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithSpareAtLastFrame() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(1,7);
		Frame f2 = new Frame(2,6);
		Frame f3 = new Frame(2,5);		
		Frame f4 = new Frame(4,5);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(6,3);
		//1st spare frame
		Frame f9 = new Frame(3,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		g.setFirstBonusThrow(8);
		
		assertEquals(92, g.calculateScore());
		
	}
	
	@Test(expected = BowlingException.class)
	public void testGameShouldNotReturnScoreWithSpareAtLastFrame() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(1,7);
		Frame f2 = new Frame(2,6);
		Frame f3 = new Frame(2,5);		
		Frame f4 = new Frame(4,5);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(6,3);
		//1st spare frame
		Frame f9 = new Frame(3,7);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		g.setFirstBonusThrow(11);
		
		g.calculateScore();
		
	}
	
	@Test
	public void testGameShouldReturnScoreWithStrikeAtLastFrame() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(1,8);
		Frame f1 = new Frame(1,7);
		Frame f2 = new Frame(2,6);
		Frame f3 = new Frame(2,5);		
		Frame f4 = new Frame(4,5);
		Frame f5 = new Frame(0,9);
		Frame f6 = new Frame(1,6);
		Frame f7 = new Frame(5,3);
		Frame f8 = new Frame(6,3);
		//1st spare frame
		Frame f9 = new Frame(10,0);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		g.setFirstBonusThrow(3);
		g.setSecondBonusThrow(6);
		
		assertEquals(93, g.calculateScore());
		
	}
	
	@Test
	public void testGameShouldReturnPerfectScore() throws BowlingException {

		Game g = new Game();
		
		Frame f0 = new Frame(10,0);
		Frame f1 = new Frame(10,0);
		Frame f2 = new Frame(10,0);
		Frame f3 = new Frame(10,0);		
		Frame f4 = new Frame(10,0);
		Frame f5 = new Frame(10,0);
		Frame f6 = new Frame(10,0);
		Frame f7 = new Frame(10,0);
		
		
		Frame f8 = new Frame(10,0);
		Frame f9 = new Frame(10,0);
		
		g.addFrame(f0);
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		
		g.setFirstBonusThrow(10);
		g.setSecondBonusThrow(10);
		
		assertEquals(300, g.calculateScore());
		
	}
	
	
	
}
