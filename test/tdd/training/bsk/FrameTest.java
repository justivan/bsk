package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testFrameShouldReturnFirstThrow() throws Exception{

		Frame f = new Frame(1,8);
		
		assertEquals(1, f.getFirstThrow());
		
		
	}
	
	@Test
	public void testFrameShouldReturnSecondThrow() throws Exception{
		
		Frame f = new Frame(1,8);
		
		assertEquals(8, f.getSecondThrow());
		
	}
	
	@Test
	public void testFrameShouldReturnScore() throws Exception{
		
		Frame f = new Frame(1,8);
		
		assertEquals(9, f.getScore());
		
	}

}
