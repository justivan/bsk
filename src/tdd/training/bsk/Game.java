package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	ArrayList<Frame> gameFrames = null;
		
	private int firstBonus = 0;
	private int secondBonus = 0;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {

		gameFrames = new ArrayList<>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		gameFrames.add(frame);	
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		return gameFrames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		
		if(firstBonusThrow >= 0 && firstBonusThrow <= 10) {
			firstBonus = firstBonusThrow;
		} else {
			throw new BowlingException("Invalid bonus on first bonus throw!");
			
		}
		
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		
		if(secondBonusThrow >= 0 && secondBonusThrow <= 10) {
			secondBonus = secondBonusThrow;
		} else {
			throw new BowlingException("Invalid bonus on first bonus throw!");
			
		}
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		if( gameFrames.isEmpty() ) {
			
			return 0;
			
		} else {
			
			int result = 0;
			
			for(int i = 0; i < gameFrames.size(); i++) {
				
				if( this.getFrameAt(i).isSpare() ) {
					
					result += calculateSparePoints(i);
					
				} else if ( this.getFrameAt(i).isStrike() ) {
					
					result += calculatePointsAfterStrike(i);

				} else {
					
					result += this.getFrameAt(i).getScore();
				}
										
			}

			return result;
		}
	
	}
	
	/**
	 * It returns the points for a Spare Frame
	 * 
	 * @return The points for a Spare Frame
	 * @throws BowlingException
	 */
	public int calculateSparePoints(int index) throws BowlingException{
		
		if( index == (gameFrames.size() - 1) ) {
			this.getFrameAt(index).setBonus( this.getFirstBonusThrow() );
		} else {
			this.getFrameAt(index).setBonus( gameFrames.get(index+1).getFirstThrow() );
		}

		return this.getFrameAt(index).getScore();
	}
	
	/**
	 * It returns the points for a Strike Frame with a Subsequent Spare Frame
	 * 
	 * @return The points for a Strike Frame with a Subsequent Spare Frame
	 * @throws BowlingException
	 */
	public int calculateStrikePointsWithSubsequentSpareFrame(int index) throws BowlingException {
		
		this.getFrameAt(index).setBonus( gameFrames.get(index+1).getFirstThrow() + gameFrames.get(index+1).getSecondThrow());
		
		return this.getFrameAt(index).getScore();

	}
	
	/**
	 * It returns the points for a Strike Frame
	 * 
	 * @return The points for a Strike Frame
	 * @throws BowlingException
	 */
	public int calculateStrikePoints(int index) throws BowlingException {

		this.getFrameAt(index).setBonus( gameFrames.get(index+1).getScore() );

		return this.getFrameAt(index).getScore();

	}
	
	/**
	 * It check the subsequent frame for a Strike one and calculates points
	 * 
	 * @return The points for a Strike Frame
	 * @throws BowlingException
	 */
	public int calculatePointsAfterStrike(int index) throws BowlingException {
		
		if(index == 9 || gameFrames.get(index+1).isStrike()) {
			return calculateStrikePointsWithSubsequentStrikeFrame(index);
		}
		
		
		if( gameFrames.get(index+1).isSpare() ) {
			
			return calculateStrikePointsWithSubsequentSpareFrame(index);
			
		} 
		
		return calculateStrikePoints(index);
		
		
	}
	
	/**
	 * It returns the points for a Strike Frame with a Subsequent Strike Frames
	 * 
	 * @return The points for a Strike Frame with a Subsequent Strike Frames
	 * @throws BowlingException
	 */
	public int calculateStrikePointsWithSubsequentStrikeFrame(int index) throws BowlingException {
		
		if( index == 9 ) {
			this.getFrameAt(index).setBonus( ( this.getFirstBonusThrow() + this.getSecondBonusThrow() ) );			
			return this.getFrameAt(index).getScore();
		} else {
			
			if(gameFrames.get(index+1).isStrike()) {
				
				if(index == 8) {			
					this.getFrameAt(index).setBonus( gameFrames.get(index+1).getFirstThrow() + firstBonus );			
					return this.getFrameAt(index).getScore();	
					
				} else if(gameFrames.get(index+2).isStrike()) {
					this.getFrameAt(index).setBonus( gameFrames.get(index+1).getScore() + gameFrames.get(index+2).getScore() );			
					return this.getFrameAt(index).getScore();		
				} else {
					this.getFrameAt(index).setBonus( gameFrames.get(index+1).getScore() + gameFrames.get(index+2).getFirstThrow() );			
					return this.getFrameAt(index).getScore();	
				}
				
				
			} //fine index+1
			else {

				this.getFrameAt(index).setBonus( gameFrames.get(index+1).getScore()  );
				return this.getFrameAt(index).getScore();
			}
			
		} 
		
		
		
	}
		
	
	/**
	 * It returns the points for a Spare Frame with a Subsequent Spare Frames
	 * 
	 * @return The points for a Spare Frame with a Subsequent Spare Frames
	 * @throws BowlingException
	 */
	public int calculateSparePointsWithSubsequentSpareFrames(int index) throws BowlingException {
		
		int tmpIndex = index;
		int tmpPoints = 0;
		
		if(gameFrames.get(tmpIndex+1).isSpare()) {
			tmpPoints += gameFrames.get(tmpIndex+1).getFirstThrow();
		}

		this.getFrameAt(index).setBonus( tmpPoints );
		
		return this.getFrameAt(index).getScore();

	}
	
	

}
